from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import json 
import requests 

def get_photo(city, state):
     # Create a dictionary for the headers to use in the request
    headers = {
        "Authorization": PEXELS_API_KEY
    }
    params = { 
        "query": f"{city}, {state}", 
        "per_page": "1"
    }

    # Create the URL for the request with the city and state
    url = f"https://api.pexels.com/v1/search"

    # Make the request
    response = requests.get(url, headers=headers, params=params)

    # Parse the JSON response
    api_dict = response.json() 

    # Return a dictionary that contains a `picture_url` key and
    #   one of the URLs for one of the pictures in the response
    return api_dict['photos'][0]['src']['original']


def get_weather_data(city, state):
    # Create the URL for the geocoding API with the city and state
    url = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},US&appid={OPEN_WEATHER_API_KEY}"

    # Make the request
    response = requests.get(url)
    # Parse the JSON response
    location = response.json()

    # Get the latitude and longitude from the response
    lat = location[0]["lat"]
    lon = location[0]["lon"]

    # Create the URL for the current weather API with the latitude and longitude
    weather_url = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}&units=imperial"

    # Make the request
    weather_response = requests.get(weather_url)

    # Parse the JSON response
    weather_data = weather_response.json() 

    # Get the main temperature and the weather's description and put them in a dictionary
    weather_dict = {
        'weather': weather_data["weather"][0]["main"], 
        'temp': weather_data["main"]['temp'], 
    }

    # Return the dictionary
    return weather_dict
from django.http import JsonResponse

from .models import Conference, Location, State
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
import json
from .acls import get_photo, get_weather_data


class ConferenceListEncoder(ModelEncoder): 
    model = Conference
    properties = [
        "name", 
    ]

@require_http_methods(["GET", "POST"])
def api_list_conferences(request):
    if request.method == "GET": 
        conferences = Conference.objects.all()
        return JsonResponse(
            {"conferences": conferences},
            encoder=ConferenceListEncoder,
        )
    else: 
        content = json.loads(request.body)

        try: 
            location = Location.objects.get(id=content["location"])
            content["location"]= location 
        except Location.DoesNotExist: 
            return JsonResponse ( 
                {"message": "Invalid location id"}, 
                status=400, 
            )
        
        conference = Conference.objects.create(**content)
        return JsonResponse(
            conference, 
            encoder=ConferenceDetailEncoder, 
            safe=False, 
        )

class LocationListEncoder(ModelEncoder): 
    model = Location 
    properties = ["name"]

class ConferenceDetailEncoder(ModelEncoder):
    model = Conference
    properties = [
        "name",
        "description",
        "max_presentations",
        "max_attendees",
        "starts",
        "ends",
        "created",
        "updated",
        "location", 
    ]
    encoders = {
        "location": LocationListEncoder(), 
    }

@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_conference(request, id):
    """
    Returns the details for the Conference model specified
    by the id parameter.

    """
    if request.method == "GET": 
        conference = Conference.objects.get(id=id)

        #get weather data from OpenWeather API 
        weather_data = get_weather_data(conference.location.city, conference.location.state)

        return JsonResponse(
            {"conference": conference, "weather": weather_data}, 
            encoder=ConferenceDetailEncoder, safe=False
        )
    elif request.method == "DELETE": 
        count, _ = Conference.objects.filter(id=id).delete() 
        return JsonResponse({"deleted": count > 0})
    else: 
        content = json.loads(request.body)

        Conference.objects.filter(id=id).update(**content)

        conference = Conference.objects.get(id=id)
        return JsonResponse(
            conference, 
            encoder=ConferenceDetailEncoder, 
            safe=False, 
        )


@require_http_methods(["GET", "POST"])
def api_list_locations(request):
    """
    Lists the location names and the link to the location.
    """
    if request.method == "GET":  
        locations = Location.objects.all()
        return JsonResponse(
            {"locations": locations}, 
            encoder=LocationListEncoder, 
        ) 
    else: 
        content = json.loads(request.body)

        #Get the state object and put it in the content dict 
        try: 
            state= State.objects.get(abbreviation=content["state"])
            content["state"] = state
        except State.DoesNotExist: 
            return JsonResponse( 
                {"message": "Invalid state abbreviation"}, 
                status=400, 
            )
        
        #Get state's full name 
        state_full_name = state.name 
        print(state_full_name)

        # Use the city and state's abbreviation in the content dictionary
        # to call the get_photo ACL function
        picture = get_photo(content["city"], state_full_name)

        # Use the returned dictionary to update the content dictionary
        content["picture_url"] = picture 

        location = Location.objects.create(**content) 
        return JsonResponse(
            location, 
            encoder=LocationDetailEncoder, 
            safe=False
        )



class LocationDetailEncoder(ModelEncoder): 
    model = Location
    properties = [
        "name", 
        "city", 
        "room_count", 
        "created", 
        "updated", 
        "picture_url"
    ]
    
    def get_extra_data(self, o): 
        return{ "state": o.state.abbreviation }

@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_location(request, id):
    """
    Returns the details for the Location model specified
    by the id parameter.
    """
    if request.method == "GET": 
        location = Location.objects.get(id=id)
        return JsonResponse(
            location, 
            encoder= LocationDetailEncoder, 
            safe= False, 
        )
    elif request.method == "DELETE": 
        count, _ = Location.objects.filter(id=id).delete() 
        return JsonResponse({"deleted": count > 0})
    else: 
        content = json.loads(request.body)
        try: 
            if "state" in content: 
                #Combined two lines 
                content["state"] = State.objects.get(abbreviation=content["state"])
        except State.DoesNotExist: 
            return JsonResponse(
                status=400, 
            )
    Location.objects.filter(id=id).update(**content)

    location = Location.objects.get(id=id)
    return JsonResponse( 
        location, 
        encoder=LocationDetailEncoder, 
        safe=False, 
    )